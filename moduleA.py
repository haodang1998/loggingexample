import logging

#get log from main moduleA
logger=logging.getLogger('logingglearn')
def hello():
    logger.debug('debug out')
    logger.warning('warning out')
    logger.info('info out')
    logger.error('error out')
    logger.critical('critical out')