import logging
from logging.handlers import RotatingFileHandler

# Constant for limit the log file size, and number of backups
MAX_LOG_SIZE_BYTES = 2048
BACKUP_COUNT = 3
# Define filter classes.
class DebugOnly(object):
    def filter(self, logRecord):
        return logRecord.levelno == logging.DEBUG
class ErrorOnly(object):
    def filter(self, logRecord):
        return logRecord.levelno == logging.ERROR


# Create logger then set a log level for it.
logger = logging.getLogger('logingglearn')
logger.setLevel(logging.DEBUG)

# Create log handlers.

# console
# PRINT DEBUG ONLY
consoleDebugOnlyHandler = logging.StreamHandler()

# file
# PRINT INFO->CRITICAL
fileFromInfoHandler = RotatingFileHandler(
    'monitor.log', mode = 'w', maxBytes = MAX_LOG_SIZE_BYTES, backupCount = BACKUP_COUNT) # Mode: 'a' is append. 'w' is write.
# PRINT ERROR ONLY
fileErrorOnlyHandler = RotatingFileHandler(
    'error.log', mode = 'w', maxBytes = MAX_LOG_SIZE_BYTES, backupCount = BACKUP_COUNT)

# Set custom log format to hanlders.
NEW_FORMAT = '[%(asctime)s] - [%(levelname)s] - [%(filename)s] - [%(funcName)s()] - [%(lineno)d]- %(message)s'
consoleDebugOnlyHandler.setFormatter(logging.Formatter(NEW_FORMAT))
fileFromInfoHandler.setFormatter(logging.Formatter(NEW_FORMAT))
fileErrorOnlyHandler.setFormatter(logging.Formatter(NEW_FORMAT))

# SET LEVEL AND FILTER

#  setLevel
#      |
#      |
#      V
# |  DEBUG   |   INFO   |   WARN   |  ERROR   | CRITICAL |
#      ^
#      |
#      |
#   filter
consoleDebugOnlyHandler.setLevel(logging.DEBUG)
consoleDebugOnlyHandler.addFilter(DebugOnly())  # JUST DEBUG

# Set DEBUG level then add DebugOnly filter.
#             setLevel
#                 |
#                 |
#                 V
# |  DEBUG   |   INFO   |   WARN   |  ERROR   | CRITICAL |
fileFromInfoHandler.setLevel(logging.INFO)  # INFO- > CRITICAL


# Set DEBUG level then add DebugOnly filter.
#                                   setLevel
#                                       |
#                                       |
#                                       V
# |  DEBUG   |   INFO   |   WARN   |  ERROR   | CRITICAL |
#                                       ^
#                                       |
#                                       |
#                                    filter
fileErrorOnlyHandler.setLevel(logging.ERROR)
fileErrorOnlyHandler.addFilter(ErrorOnly())  # JUST ERROR


# Add handlers to logger to receive log record.
logger.addHandler(consoleDebugOnlyHandler)
logger.addHandler(fileFromInfoHandler)
logger.addHandler(fileErrorOnlyHandler)


# demo log
logger.debug('debug out')
logger.warning('warning out')
logger.info('info out')
logger.info('Log file saved in %s', fileFromInfoHandler.baseFilename)
logger.error('error out')
logger.critical('critical out')
#log from other module
import moduleA
moduleA.hello()